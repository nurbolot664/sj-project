import os
import subprocess
import tempfile

from django.db.models import Q
from django.http import FileResponse
from drf_yasg import openapi
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser
from django.core.files.storage import default_storage

from rest_framework.viewsets import ViewSet
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action

from drf_yasg.utils import swagger_auto_schema

from src.apps.patient.models.patient_models import Patient, PatientInfo
from src.apps.patient.models.info_models import (
    AnamnesisLife,
    SomaticStatus,
    NeurologicalStatus,
    MentalStatus,
)
from src.config.settings import BASE_DIR
from ..serializers import (
    PatientCreateSerializer,
    PatientListSerializer,
    PatientDetailSerializer,
    PatientDetailPatchSerializer,
)
from ..service import CustomPagination
from ..tasks import backup_database


class PatientViewSet(ViewSet):
    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=PatientCreateSerializer)
    def create(self, request):
        # Handle POST request to create a new instance
        serializer = PatientCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request):
        # Handle GET request to list all instances
        search_query = request.query_params.get('search', None)
        queryset = Patient.objects.all().order_by('-updated_at')

        if search_query:
            keywords = search_query.split()
            q_objects = Q()
            for keyword in keywords:
                q_objects |= (
                        Q(name__icontains=keyword) |
                        Q(surname__icontains=keyword) |
                        Q(patronymic__icontains=keyword)
                )
            queryset = queryset.filter(q_objects)

        # Apply pagination using the custom pagination class
        paginator = CustomPagination()
        paginated_queryset = paginator.paginate_queryset(queryset, request)

        serializer = PatientListSerializer(paginated_queryset, many=True)

        return paginator.get_paginated_response(serializer.data)

    def retrieve(self, request, pk=None):
        # Handle GET request to retrieve a single instance
        patient = get_object_or_404(Patient, id=pk)
        serializer = PatientDetailSerializer(patient, context={'request': request})
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    @swagger_auto_schema(request_body=PatientDetailPatchSerializer)
    def partial_update(self, request, pk=None):
        # Handle PATCH request to partially update an instance
        patient = get_object_or_404(Patient, id=pk)
        serializer = PatientDetailPatchSerializer(patient, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def destroy(self, request, pk=None):
        # Handle DELETE request to delete an instance
        patient = get_object_or_404(Patient, id=pk)
        patient.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class StatusListView(ViewSet):

    @action(detail=False, methods=['GET'])
    def education_list(self, request) -> Response:
        queryset = AnamnesisLife.EDUCATIONS_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def family_list(self, request) -> Response:
        queryset = AnamnesisLife.FAMILY_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def arrives_list(self, request) -> Response:
        queryset = PatientInfo.STATUS_CHOICES_ARRIVES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def conditions_list(self, request) -> Response:
        queryset = PatientInfo.STATUS_CHOICES_CONDITIONS
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def situation_list(self, request) -> Response:
        queryset = SomaticStatus.CONDITION_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def nutrition_list(self, request) -> Response:
        queryset = SomaticStatus.CATEGORY_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def skin_list(self, request) -> Response:
        queryset = SomaticStatus.SKIN_TYPE_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def availability_list(self, request) -> Response:
        queryset = SomaticStatus.AVAILABILITY_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def traces_list(self, request) -> Response:
        queryset = SomaticStatus.TRACES_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def conjunctiva_list(self, request) -> Response:
        queryset = SomaticStatus.CONJUNCTIVA_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def wheezing_list(self, request) -> Response:
        queryset = SomaticStatus.WHEEZING_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def heart_list(self, request) -> Response:
        queryset = SomaticStatus.HEART_TONES_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def pupils_list(self, request) -> Response:
        queryset = NeurologicalStatus.PUPILS_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def meningeal_list(self, request) -> Response:
        queryset = NeurologicalStatus.MENINGEAL_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def views_list(self, request) -> Response:
        queryset = MentalStatus.VIEW_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def filling_list(self, request) -> Response:
        queryset = SomaticStatus.FILLING_STATUS_CHOICES
        status_list = [{'id': role[0], 'title': role[1]} for role in queryset]
        return Response(status_list, status=status.HTTP_200_OK)



class DatabaseBackupView(APIView):
    def get(self, request, *args, **kwargs):
        backup_dir = os.path.join(BASE_DIR, 'db_backups')

        try:
            latest_backup = max(
                (os.path.join(backup_dir, f) for f in os.listdir(backup_dir)),
                key=os.path.getctime
            )
        except ValueError:
            return Response({"error": "No backup file found."}, status=404)

        if os.path.exists(latest_backup):
            print(latest_backup)
            return FileResponse(open(latest_backup, 'rb'), as_attachment=True)
        else:
            return Response({"error": "No backup file found."}, status=404)


class RestoreDatabaseView(APIView):
    parser_classes = [MultiPartParser]

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                'backup_file',
                openapi.IN_FORM,
                description="SQL-файл для восстановления базы данных",
                type=openapi.TYPE_FILE,
                required=True
            )
        ],
        responses={
            200: 'Database restored successfully',
            400: 'No file uploaded',
            500: 'Failed to restore database',
        }
    )
    def post(self, request, *args, **kwargs):
        # Получаем загруженный файл из запроса
        backup_file = request.FILES.get('backup_file')
        if not backup_file:
            return Response({"error": "No file uploaded."}, status=400)

        os.environ['PGPASSWORD'] = os.getenv('DB_PASSWORD')

        try:
            with tempfile.NamedTemporaryFile(delete=False) as temp_file:
                for chunk in backup_file.chunks():
                    temp_file.write(chunk)
                temp_file.flush()  # Обеспечиваем, что данные полностью записаны

            with open(temp_file.name, 'rb') as f:
                subprocess.run(
                    [
                        "psql",
                        "-U", os.getenv('DB_USER'),
                        "-h", os.getenv('DB_HOST'),
                        "-p", os.getenv('DB_PORT'),
                        os.getenv('DB_NAME')
                    ],
                    stdin=f,
                    check=True
                )
            return Response({"message": "Database restored successfully."})
        except subprocess.CalledProcessError as e:
            return Response({"error": f"Failed to restore database: {e}"}, status=500)
        finally:
            del os.environ['PGPASSWORD']
            # Удаляем временный файл
            if os.path.exists(temp_file.name):
                os.remove(temp_file.name)
