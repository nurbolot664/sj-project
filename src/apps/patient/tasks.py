import os
from dotenv import load_dotenv

from src.config.settings import BASE_DIR
import subprocess

from datetime import datetime

from celery import shared_task
from django.db.models import Max
from django.utils import timezone
from .models.patient_models import PatientInfo
import logging

logger = logging.getLogger(__name__)
load_dotenv()

@shared_task
def update_patient_status():
    latest_discharge_dates = (
        PatientInfo.objects.values('patient')
        .annotate(
            latest_discharge=Max(
                'date_of_discharge'
            )
        )
    )
    for patient_info_true in latest_discharge_dates:
        patient_info_instance = (
            PatientInfo.objects
            .filter(patient=patient_info_true['patient'])
            .first()
        )
        latest_patient_info = (
            PatientInfo.objects
            .filter(patient=patient_info_instance.patient)
            .latest('date_of_discharge')
        )
        if latest_patient_info.date_of_discharge < timezone.now():
            latest_patient_info.patient.in_hospital = False
        else:
            latest_patient_info.patient.in_hospital = True

        latest_patient_info.patient.save()


@shared_task
def backup_database():
    backup_dir = os.path.join(BASE_DIR, 'db_backups')

    os.makedirs(backup_dir, exist_ok=True)

    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    backup_file = os.path.join(backup_dir, f"db_backup_{timestamp}.sql")

    os.environ['PGPASSWORD'] = os.getenv('DB_PASSWORD')
    logger.info(f"Starting backup: {backup_file}")
    logger.info(f"Database credentials: {os.getenv('DB_USER')}@{os.getenv('DB_HOST')}:{os.getenv('DB_PORT')}/{os.getenv('DB_NAME')}")

    try:
        subprocess.run(
            [
                "pg_dump",
                "-U", os.getenv('DB_USER'),
                "-h", os.getenv('DB_HOST'),
                "-p", os.getenv('DB_PORT'),
                os.getenv('DB_NAME'),
                "-f", backup_file
            ],
            check=True
        )
        logger.info(f"Backup completed successfully: {backup_file}")

    except subprocess.CalledProcessError as e:
        logger.error(f"Error occurred while creating backup: {e}")
    finally:
        del os.environ['PGPASSWORD']
